#!/bin/bash
filename=$USER-i3lock.png

# -s pixel_size   adjust the obfuscation strength (default=9)
# --size=pixel_size
# -f --fuzzy    add a blur effect
xwobf -s 9 /tmp/$filename
i3lock -i /tmp/$filename
